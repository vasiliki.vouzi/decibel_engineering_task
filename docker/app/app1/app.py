from flask import Flask,Response, request,jsonify
from itertools import count
import numpy as np
import pandas as pd
from scipy.spatial.distance import mahalanobis

def create_app():
    flask_app =Flask(__name__)
    return flask_app

app = create_app()

@app.route('/', methods=['GET'])
def hello():
    return "Hello!! Please provide a valid endpoint!"

@app.route('/get_anomalous_data', methods=['POST'])
def get_anomalous_data():

    window_size=50

    #get request data to string
    incoming=str(request.get_data(), 'utf-8')

    #convert to data points
    data_points=incoming.strip(" ").replace("(","").split(")")
    data_ls_nofilter = list(map(lambda x: tuple(x.split(",")), data_points))

    data_ls=list(filter(lambda y: y[0]!='' ,data_ls_nofilter))

    app.logger.info("received: {}".format(data_ls))
    print("received: {}".format(data_ls))

    #clean NaN values and convert object columns to float
    data_dirty=pd.DataFrame(data_ls,dtype=float)
    data=data_dirty.dropna().astype(float)

    app.logger.info("Data Frame built: {}".format((data.head())))

    # if the data values provided are less than 50 then return 202 error
    if data.size < 50:
        return Response("Please provide more than 50 data points!", status=202, mimetype='html')

    #get window size from url
    if request.args.get("window")!=None:
        if request.args.get("window").isdigit():
            window_size = int(request.args.get("window"))

    app.logger.info("window size is:{}".format(window_size))

    try:
        result=get_anomalous_values(data, window_size)
    except Exception as e:
        return Response("Internal Server Error : {}".format(e.args), status=500, mimetype='html')
    except RuntimeWarning as e:
        return Response("Please check the data points provided!!".format(e.args), status=202, mimetype='html')

    app.logger.info("result:{}".format(result))

    return jsonify(result)


def get_anomalous_values(data, window_size, prob=0.99):
    """
    return a list of anomalous values, i.e. the ones that exceed md times
    in terms of Mohalanobis distance the expected multivariate average. Both
    multivariate average and Mahalanobis distance are calculated considering
    the moving windows, i.e. the value computed considering window_size
    neighbours, moving the window for each value of the series.

    data : pandas.core.frame.DataFrame
    window_size: int
    md: float

    return: list
    """

    # under normal hypotesis, the Mohalanobis dinstance is Chi-squared
    # distributed
    threshold = np.sqrt(-2 * np.log(1 - prob))

    # calculate the moving window for each point, and report the anomaly if
    # the distance of the idx-th point is greater than md times the mahalanobis
    # distance
    # result= [(p['idx'], p['value']) for p in nd_rolling(data, window_size)
    #         if mahalanobis(p['value'], p['window_avg'],
    #                        np.linalg.inv(p['window_cov'])) > threshold]
    #
    result= [{ str(p['idx']): p['value'].tolist()} for p in nd_rolling(data, window_size) #[(p['idx'], p['value'])
            if mahalanobis(p['value'], p['window_avg'],
                           np.linalg.inv(p['window_cov'])) > threshold]

    return result

def nd_rolling(data, window_size):
    """
    Generate the moving window

    data: pandas.core.frame.DataFrame
    window_size: float

    yield: tuple
    """

    # calculate the moving window for each idx-th point
    #convert data to tuples
    sample = list(zip(count(), data.values[:, 0], data.values[:, 1]))

    for idx in range(0, len(sample)):
        #construct the id of the data point
        idx0 = idx if idx - window_size < 0 else idx - window_size
        #construct the window
        window = [it for it in sample
                  if it[0] >= idx0 or it[1] <= idx0 + window_size]
        #get the x and y values in separate matrices to calc the mean
        x = np.array([it[1] for it in window])
        y = np.array([it[2] for it in window])

        yield {'idx': idx,
               'value': np.array(tuple(sample[idx][1:])),
               'window_avg': np.array((np.mean(x), np.mean(y))),
               'window_cov': np.cov(x, y, rowvar=0)}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)