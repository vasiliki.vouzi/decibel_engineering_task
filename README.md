## Part 1 - Algorithm

The code consumes tuples of data points that represent a point in a X,Y
coordinate system. It organizes them in moving (rolling) window of a 
configured size (window_size) and calculates for each point in each 
window two statistical measures the mean and the covariance. The mean 
 is calculated separately in all the x and all the y values of the data
 points (np.mean(x),np.mean(y)).  These  measures are used to calculate 
 the mahalanobis distance each point has from the expected multivariate 
 average of the data of the moving window. 
 
 If the mahalanobis distance 
 is greater than the threshold (md times with a=0.01) calculated then 
 the point is reported as an anomaly. The threshold is calculated as 
 such based on the consideration that the mahalanobis distance distribution
 follows the chi-squared distribution. 
  
  The mahalanobis distance is calculated based on the distance of each 
  data point from the mean of each window and its inverted covariance 
  matrix.

## Part 2 - Code, Window Hypothesis and Dockerization

### Code
The main app is located in app.py. 

The endpoint requested from the assignment calls the function get_anomalous_data.

Some changes were made to the functions provided by the data 
scientists in order to facilitate the output format.

The boot.sh script along with the requirements.txt is used for the dockerization needs

### Window Hypothesis

The default window was set to 50 and proposed to get the window size either 
from the body of the request or from the url. If it is to be get from the 
request body a more structured body should be considered i.e. application/json 
instead of text/plain.
 

### Dockerization

The main code is in
```
./docker/app/app1/app.py 
```
It has been tested under windows 10 with conda and works fine. During 
dockerization I encountered a problem with numpy's sqrt function that I 
could not solve. It is probably related to [issue](https://github.com/numpy/numpy/issues/11448)
I tried using various alternatives as base in my dockerfile suchs as 
conda or other python versions with different numpy versions at
 the `requirements.txt` (`requirements_with_versions.txt`) but could not get it to work.

You can find the dockerfile in order to rebuild the image in 
```
./docker/app/app1/dockerfile 
```
and the conda dockerfile is 
```
./docker/app/app1/dockerfile_conda 
```
or you could use the docker-compose.yml provided in the same folder

## Part 3- Deploy the designed pipeline

For the deployment a t2.micro was used in AWS cloud. The AMI used is
ami-05c1fa8df71875112 . The service is running and you can find it on
```
http://52.15.183.147:80/get_anomalous_data
```
I will keep the service up for a week or so. If you visit the url please 
notify me in order to close down the server.

You can find all the available code in this git repo.

I have tried to play around a bit with ansible and its playbooks in 
order to get a new instance in aws running in a new vpc with the service
on but could not successfully test it.